class BandsController < ApplicationController
  def new
    @band = Band.new
  end

  def create
    @band = Band.new(band_params)
    if @band.save
      flash[:success] = "Band has been added."
      redirect_to @band
    else
      render 'new'
    end
  end
  
  def show
    @band = Band.find(params[:id])
    @albums = @band.albums
  end
  
  def index
    @bands = Band.all
  end
  
  def edit
    @band = Band.find(params[:id])
  end

  def update
    @band = Band.find(params[:id])
    if @band.update_attributes(band_params)
      flash[:success] = "Band updated"
      redirect_to @band
    else
      render 'edit'
    end
  end
  
  def destroy
    Band.find(params[:id]).destroy
    flash[:success] = "Band deleted"
    redirect_to bands_path
  end
  
  private

    def band_params
      params.require(:band).permit(:name, :biography, :photo)
    end
end