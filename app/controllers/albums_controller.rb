class AlbumsController < ApplicationController
  def new
    @album = Album.new
    @bands = Band.all
  end

  def create
    @album = Album.new(album_params)
    @bands = Band.all
    
    if @album.save
      flash[:success] = "Album has been added."
      redirect_to @album
    else
      render 'new'
    end
  end
  
  def show
    @album = Album.find(params[:id])
    @band = Band.find(@album.band_id)
  end
  
  def index
    @albums = Album.all
  end
  
  def edit
    @album = Album.find(params[:id])
    @bands = Band.all
  end

  def update
    @album = Album.find(params[:id])
    if @album.update_attributes(album_params)
      flash[:success] = "Album updated"
      redirect_to @album
    else
      render 'edit'
    end
  end
  
  def destroy
    Album.find(params[:id]).destroy
    flash[:success] = "Album deleted"
    redirect_to albums_path
  end
  
  private

    def album_params
      params.require(:album).permit(:name, :info, :photo, :band_id)
    end
end