class Album < ActiveRecord::Base
  belongs_to :band
  validates :band, presence: true
  validates :name, presence: true
end
