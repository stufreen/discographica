class Band < ActiveRecord::Base
  has_many :albums, dependent: :destroy
  validates :name,  presence: true, 
                    uniqueness: true,
                    case_sensitive: false
                    
  def get_name
    self.name
  end
  
  def self.get_name_by_id the_id
    Band.find(params[the_id])
  end
end
