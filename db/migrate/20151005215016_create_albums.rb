class CreateAlbums < ActiveRecord::Migration
  def change
    create_table :albums do |t|
      t.string :name
      t.string :photo
      t.string :info
      t.references :band

      t.timestamps null: false
    end
  end
end
